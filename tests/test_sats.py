import math
from skyfield.api import load
from spytnik import sats
import unittest


class TestSats(unittest.TestCase):
    """
    Test Sats
    """

    ts = load.timescale()

    def test_load_tle_file(self):
        """
        Test loading of a TLE file
        """
        tles = sats.load_sats("tests/data/test.tle")

        names = tles.names()
        self.assertEqual(len(names), 5)
        self.assertEqual(
            names, ["Canyon 2", "Rhyolite 1", "Canyon 6", "NOSS 1 (A)", "DSP F7 Cover"]
        )

        ids = tles.ids()
        self.assertEqual(len(ids), 5)
        self.assertEqual(ids, [3889, 4418, 7963, 8818, 9856])

        sat_by_id = tles.sat_by_id(7963)
        epoch = self.ts.utc(year=2020, day=19.44661353)
        self.assertEqual(sat_by_id.name, "Canyon 6")
        self.assertEqual(sat_by_id.epoch.ut1, epoch.ut1)
        self.assertEqual(sat_by_id.model.satnum, 7963)
        self.assertEqual(sat_by_id.model.classification, "U")
        self.assertEqual(sat_by_id.model.intldesg, "75055A")
        self.assertEqual(
            sat_by_id.model.epochyr, 20
        )  # From doc it should be 4 digit year
        self.assertEqual(sat_by_id.model.bstar, 0)
        self.assertEqual(sat_by_id.model.elnum, 0)
        self.assertAlmostEqual(
            sat_by_id.model.inclo, 16.1777 / 360 * (2 * math.pi), places=14
        )
        self.assertAlmostEqual(
            sat_by_id.model.nodeo, 292.0988 / 360 * (2 * math.pi), places=14
        )
        self.assertEqual(sat_by_id.model.ecco, 0.1364095)
        self.assertAlmostEqual(
            sat_by_id.model.argpo, 349.2540 / 360 * (2 * math.pi), places=14
        )
        self.assertAlmostEqual(
            sat_by_id.model.mo, 8.1103 / 360 * (2 * math.pi), places=14
        )
        self.assertAlmostEqual(
            sat_by_id.model.no_kozai, 1.00297580 / 1440 * (2 * math.pi), places=14
        )
        self.assertEqual(sat_by_id.model.revnum, 0)

        sat_by_name = tles.sat_by_name("Rhyolite 1")
        epoch = self.ts.utc(year=2021, day=151.91249899)
        self.assertEqual(sat_by_name.name, "Rhyolite 1")
        self.assertEqual(sat_by_name.epoch.ut1, epoch.ut1)
        self.assertEqual(sat_by_name.model.satnum, 4418)
        self.assertEqual(sat_by_name.model.classification, "U")
        self.assertEqual(sat_by_name.model.intldesg, "70046A")
        self.assertEqual(
            sat_by_name.model.epochyr, 21
        )  # From doc it should be 4 digit year
        self.assertEqual(sat_by_name.model.bstar, 0)
        self.assertEqual(sat_by_name.model.elnum, 0)
        self.assertAlmostEqual(
            sat_by_name.model.inclo, 2.4659 / 360 * (2 * math.pi), places=14
        )
        self.assertAlmostEqual(
            sat_by_name.model.nodeo, 282.9236 / 360 * (2 * math.pi), places=14
        )
        self.assertEqual(sat_by_name.model.ecco, 0.0010125)
        self.assertAlmostEqual(
            sat_by_name.model.argpo, 170.3685 / 360 * (2 * math.pi), places=14
        )
        self.assertAlmostEqual(
            sat_by_name.model.mo, 189.6626 / 360 * (2 * math.pi), places=14
        )
        self.assertAlmostEqual(
            sat_by_name.model.no_kozai, 1.00263758 / 1440 * (2 * math.pi), places=14
        )
        self.assertEqual(sat_by_name.model.revnum, 0)

        # Check it does work when giving id as strting
        sat_by_id = tles.sat_by_id("7963")
        self.assertEqual(sat_by_id.model.satnum, 7963)
