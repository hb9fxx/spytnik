import math
from skyfield.api import load, wgs84
from spytnik import sites
import unittest


class TestSites(unittest.TestCase):
    """
    Test Sites
    """

    ts = load.timescale()

    def test_load_site_file(self):
        """
        Test loading of a site file
        """
        sites_list = sites.load_sites("tests/data/sites.txt")

        ids = sites_list.ids()
        self.assertEqual(len(ids), 4)
        self.assertEqual(ids, [1, 9, 1234, 98676])

        latlon = wgs84.latlon(58.468, 105.12, elevation_m=276)
        site = sites_list.get_site(1)
        self.assertEqual(site["acronym"], "AA")
        self.assertEqual(site["name"], "Site # 1")
        self.assertEqual(site["latlon"].latitude.degrees, latlon.latitude.degrees)
        self.assertEqual(site["latlon"].longitude.degrees, latlon.longitude.degrees)
        self.assertEqual(site["latlon"].elevation.m, latlon.elevation.m)

        latlon = wgs84.latlon(72.646, 38.788, elevation_m=17)
        site = sites_list.get_site(9)
        self.assertEqual(site["acronym"], "BB")
        self.assertEqual(site["name"], "Non octal site")
        self.assertEqual(site["latlon"].latitude.degrees, latlon.latitude.degrees)
        self.assertEqual(site["latlon"].longitude.degrees, latlon.longitude.degrees)
        self.assertEqual(site["latlon"].elevation.m, latlon.elevation.m)

        latlon = wgs84.latlon(87.67, 645.570, elevation_m=3)
        site = sites_list.get_site(1234)
        self.assertEqual(site["acronym"], "CC")
        self.assertEqual(site["name"], "Site with tabs")
        self.assertEqual(site["latlon"].latitude.degrees, latlon.latitude.degrees)
        self.assertEqual(site["latlon"].longitude.degrees, latlon.longitude.degrees)
        self.assertEqual(site["latlon"].elevation.m, latlon.elevation.m)

        latlon = wgs84.latlon(74.281, -0.630, elevation_m=192)
        site = sites_list.get_site(98676)
        self.assertEqual(site["acronym"], "DD")
        self.assertEqual(site["name"], "5 digit site")
        self.assertEqual(site["latlon"].latitude.degrees, latlon.latitude.degrees)
        self.assertEqual(site["latlon"].longitude.degrees, latlon.longitude.degrees)
        self.assertEqual(site["latlon"].elevation.m, latlon.elevation.m)

        # Check it does work when giving id as strting
        site = sites_list.get_site("1")
        self.assertEqual(site["acronym"], "AA")
