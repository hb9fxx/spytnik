from spytnik import sats, sites

from datetime import datetime, timedelta
import dateutil.parser
from enum import Enum
import numpy as np
import matplotlib.pyplot as plt
from skyfield.api import load


class Filter(Enum):
    NONE = 1
    POINT = 2
    ZONE = 3


class PassPredict:
    def __init__(self, tle):
        # Create timescale
        self.ts = load.timescale()

        # Load TLEs
        self.tles = sats.load_sats(tle)

        # Create location
        self.site = sites.load_sites().get_site()

        self.filter = Filter.NONE

    def setPointFilter(self, az, el, deg):
        self.pointFilterAz = az
        self.pointFilterEl = el
        self.pointFilterDeg = deg
        self.filter = Filter.POINT

    def setZoneFilter(self, minAz, maxAz, minEl, maxEl):
        self.zoneFilterMinAz = minAz
        self.zoneFilterMaxAz = maxAz
        if self.zoneFilterMaxAz < self.zoneFilterMinAz:
            self.zoneFilterMaxAz = self.zoneFilterMaxAz + 360.0
        self.zoneFilterMinEl = minEl
        self.zoneFilterMaxEl = maxEl
        self.filter = Filter.ZONE

    def predict(self, sat_ids, start, end, duration, min_elevation=10):
        if start:
            # TODO: add UTC timezone if not present
            t0 = self.ts.from_datetime(dateutil.parser.isoparse(start))
        else:
            t0 = self.ts.now()

        if end:
            # TODO: add UTC timezone if not present
            t1 = self.ts.from_datetime(dateutil.parser.isoparse(end))
        else:
            if not duration:
                duration = 24
            t1 = self.ts.from_datetime(t0.utc_datetime() + timedelta(hours=duration))

        passes = []

        # Get passes for each satellite
        for sat_id in sat_ids:
            sat = self.tles.sat_by_id(sat_id)
            print(sat)

            times, events = sat.find_events(
                self.site["latlon"], t0, t1, altitude_degrees=min_elevation
            )
            satPasses = self.convert2Passes(sat, times, events)

            passes.extend(satPasses)

        # Sort all passes by rise time
        passes.sort(key=lambda p: p["rise"].utc_datetime())

        # Apply filters if defined
        if self.filter == Filter.ZONE:
            passes = filter(lambda p: self.zoneFilter(p), passes)

        # List and graph passes
        for p in passes:
            print(
                p["sat"].name
                + ": Rise: "
                + p["rise"].utc_strftime("%Y-%m-%dT%H:%M:%S")
                + " Set: "
                + p["set"].utc_strftime("%Y-%m-%dT%H:%M:%S")
            )
            self.graph(p)

    def convert2Passes(self, sat, times, events):
        passes = []

        for t, e in zip(times, events):
            if e == 0:  # rise
                passes.append({"rise": t, "sat": sat})
            elif e == 1:  # culminate
                if len(passes):
                    if "culminate" in passes[-1].keys():
                        passes[-1]["culminate"].append(t)
                    else:
                        passes[-1]["culminate"] = [t]
            elif e == 2:  # set
                if len(passes):
                    passes[-1]["set"] = t

        if len(passes) and "set" not in passes[-1].keys():
            passes.pop()

        difference = sat - self.site["latlon"]

        for p in passes:
            r = p["rise"].utc_datetime()
            s = p["set"].utc_datetime()
            p["trajectory"] = self.calculateTrajectory(difference, r, s)

        return passes

    def calculateTrajectory(self, d, r, s):
        t = []

        while r < s:
            topocentric = d.at(self.ts.from_datetime(r))
            alt, az, distance = topocentric.altaz()
            t.append((az.degrees, alt.degrees, r))
            r += timedelta(seconds=15)

        topocentric = d.at(self.ts.from_datetime(s))
        alt, az, distance = topocentric.altaz()
        t.append((az.degrees, alt.degrees, s))

        return np.array(t)

    def zoneFilter(self, p):
        for az, el, t in p["trajectory"]:
            # if zone filter crosses north, max az is set as + 360°
            if az < self.zoneFilterMinAz:
                az = az + 360

            if (
                self.zoneFilterMinAz < az < self.zoneFilterMaxAz
                and self.zoneFilterMinEl < el < self.zoneFilterMaxEl
            ):
                return True

    def graph(self, p):
        az, el, t = zip(*p["trajectory"])

        # Convert to nparrays. set_rlim doesn't seem to support inverted range (90 at center, 0 at border), so convert it here
        az = np.array(np.deg2rad(az))
        el = 90.0 - np.array(el)

        ax = plt.subplot(111, projection="polar")

        if self.filter == Filter.ZONE:
            self.graphDisplayZoneFilter(ax)

        ax.plot(az, el)
        ax.scatter(az[0], el[0], s=50, c="green")
        ax.scatter(az[-1], el[-1], s=50, c="red")

        ax.set_rlim(0, 90)
        ax.set_rticks([0, 30, 60])  # Less radial ticks
        ax.set_theta_direction(-1)
        ax.set_theta_offset(np.deg2rad(90))
        ax.grid(True)

        ax.set_title(
            p["sat"].name + " (" + str(p["sat"].model.satnum) + ")", va="bottom"
        )
        plt.show()

    def graphDisplayZoneFilter(self, ax):
        # TODO: handle azMax lower when crossing north, by adding 360 to max and do a module
        az1 = np.arange(start=self.zoneFilterMinAz, stop=self.zoneFilterMaxAz, step=1)
        el1 = np.full(len(az1), self.zoneFilterMinEl)

        el2 = np.arange(start=self.zoneFilterMinEl, stop=self.zoneFilterMaxEl, step=1)
        az2 = np.full(len(el2), self.zoneFilterMaxAz)

        az3 = np.arange(start=self.zoneFilterMaxAz, stop=self.zoneFilterMinAz, step=-1)
        el3 = np.full(len(az3), self.zoneFilterMaxEl)

        el4 = np.arange(start=self.zoneFilterMaxEl, stop=self.zoneFilterMinEl, step=-1)
        az4 = np.full(len(el4), self.zoneFilterMinAz)

        az = np.deg2rad(np.concatenate((az1, az2, az3, az4)))
        el = 90.0 - np.concatenate((el1, el2, el3, el4))

        ax.plot(az, el, c="lightgrey")
