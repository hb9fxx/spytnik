import argparse

from passpredict import PassPredict


def getArgs():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "sat_ids", help="Satellite catalog id", metavar="id", type=int, nargs="+"
    )
    parser.add_argument("-t", "--tle", help="TLE file")
    parser.add_argument("-s", "--start", help="Start time")
    parser.add_argument("-e", "--end", help="Start time")
    parser.add_argument("-d", "--duration", help="Duration in hours", type=int)
    parser.add_argument(
        "-a",
        "--min-elevation-angle",
        help="Minimal elevation angle the satellite must reach",
        type=float,
        default=10,
    )
    parser.add_argument(
        "-z",
        "--zone-filter",
        metavar=("MIN_AZ", "MAX_AZ", "MIN_EL", "MAX_EL"),
        help="Zone filter",
        type=int,
        nargs=4,
    )
    return parser.parse_args()


def main(args=None):
    """The main routine."""

    args = getArgs()

    p = PassPredict.PassPredict(args.tle)

    if args.zone_filter:
        p.setZoneFilter(
            args.zone_filter[0],
            args.zone_filter[1],
            args.zone_filter[2],
            args.zone_filter[3],
        )
    p.predict(
        args.sat_ids,
        start=args.start,
        end=args.end,
        duration=args.duration,
        min_elevation=args.min_elevation_angle,
    )


if __name__ == "__main__":
    main()
