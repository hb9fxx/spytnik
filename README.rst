=========================
SPyTnik STRF Trace Viewer
=========================

A simple STRF trace viewer, showing the satellite's ground track and the AZ/EL
of the observing station of one or several STRF .dat files.

-------
Install
-------

Install from PyPi with

    $ sudo pip3 install spytnik

-----------------
Build from source
-----------------

Install SPyTnik's requirements, run the following commands::

    $ sudo apt install python3-pip python3-setuptools

Clone the repo::

    $ git clone https://gitlab.com/hb9fxx/spytnik.git

Build::

    $ sudo pip3 install .
